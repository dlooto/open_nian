/* 2014-09-14 */
alter table users_user add column `pdu` varchar(10) NOT NULL;
alter table users_user modify column `birth` date;

alter table families_familyship add column `unread_count` integer NOT NULL;
alter table regards_regard add column `status` varchar(1) NOT NULL;
alter table regards_regard add column `is_sent_now` bool NOT NULL;

alter table regards_regard add column `family_id` integer NOT NULL;
alter table regards_regard drop column author_avatar;
alter table regards_regard drop column author_nickname;
alter table regards_regard drop column author_innername;

alter table posts_post drop column author_avatar;
alter table posts_post drop column author_nickname;

alter table regards_datetip add column `creator_id` integer NOT NULL;

alter table regards_regard add column `is_checked` bool NOT NULL;
alter table families_familyship add column `new_regards_count` integer NOT NULL;

/* AUTO_INCREMENT from specified value */
alter table users_user AUTO_INCREMENT=10001000;
alter table families_family AUTO_INCREMENT=100100;

alter table users_user add column `acct_type` varchar(1) NOT NULL;

alter table  posts_comment add column  `aud_len` integer;
alter table  posts_comment add column `reply_to_id` integer;

alter table posts_post add column `pic_num` integer NOT NULL;
alter table posts_post modify column `pic_num` integer;

/* 动态发多图功能, 兼容之前版本仅一张图时pic_num=0数据 */
update posts_post set pic_num=1 where pic <> '' and pic_num = 0;