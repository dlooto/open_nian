/* 2014-12 */
alter table regards_regard modify column `family_id` integer;
alter table families_familyship drop new_regards_count;

alter table packages_apppackage modify `pkg_num` integer;
alter table packages_apppackage add column `pkg_size` integer NOT NULL;
alter table packages_apppackage add column `change_log` longtext NOT NULL;
alter table packages_downloadlog add column `pkgname` varchar(50) NOT NULL;

/*  待发送问候SQL  --存档作为参考
    sql = '''SELECT id, author_id, receiver_id, COUNT(receiver_id) count FROM regards_regard
         WHERE (status = 'C' and is_sent_now = 0 and sent_time <= now()) GROUP BY receiver_id'''
*/         
         
alter table regards_newregardmessage add column `is_pushed` bool NOT NULL;   
alter table regards_newregardmessage add column `pushed_err_count` integer NOT NULL;  

alter table regards_datetip add column `related_buddy_id` integer;
alter table regards_datetip add column `notice_type` smallint NOT NULL;
alter table packages_apppackage add column `desc` longtext NOT NULL;    

alter table regards_datetip add column `is_triggered` bool NOT NULL;
alter table regards_newregardmessage add column `msg_type` varchar(1) NOT NULL;
alter table regards_newregardmessage add column `item_content` varchar(150) NOT NULL;
alter table regards_newregardmessage add column `item_time` datetime;

alter table regards_regard add column `reply_to_id` integer;

alter table packages_apppackage add column `is_test_pkg` bool NOT NULL;

/* modify phone format bug */
alter table users_user modify `phone` varchar(18);

alter table regards_newregardmessage add column `updated_time` datetime NOT NULL;
alter table regards_datetip add column `is_checked` bool NOT NULL;

