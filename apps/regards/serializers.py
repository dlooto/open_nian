#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Jul 25, 2014, by Junn
#
from core.serializers import BaseModelSerializer, CustomPaginationSerializer
from regards.models import Regard, DateTip, SystemMsg
from rest_framework import serializers
from users.models import User

class RegardSerializer(BaseModelSerializer):
    author_avatar = serializers.SerializerMethodField('get_author_avatar')
    author_nickname = serializers.SerializerMethodField('get_author_nickname')
    
    receiver_avatar = serializers.SerializerMethodField('get_receiver_avatar')
    receiver_nickname = serializers.SerializerMethodField('get_receiver_nickname')
    
    sent_time = serializers.SerializerMethodField('str_sent_time')
    
    reply_by = serializers.SerializerMethodField('get_reply_by')

    class Meta:
        model = Regard
        fields = (
            'id', 'author', 'receiver', 'author_nickname', 'author_avatar', 
            'receiver_avatar', 'receiver_nickname', 'info', 'pic', 'aud', 'aud_len', 
            'created_time', 'type', 'sent_time', 'status', 'is_sent_now', 'is_checked',
            'reply_by'
        )
        depth = 0
        ordering = ('-sent_time', )
        
    def str_sent_time(self, obj):
        return obj.sent_time if isinstance(obj.sent_time, unicode) else obj.sent_time.strftime('%Y-%m-%d %H:%M:%S')
        #return obj.sent_time.strftime('%Y-%m-%d %H:%M:%S')
    
    def get_author_nickname(self, obj):
        user = User.objects.get_cached(obj.author_id)
        return user.nickname if user else ''
    
    def get_author_avatar(self, obj):
        user = User.objects.get_cached(obj.author_id)
        return user.avatar if user else ''    
    
    def get_receiver_avatar(self, obj):
        user = User.objects.get_cached(obj.receiver_id)
        return user.avatar if user else ''      
    
    def get_receiver_nickname(self, obj):
        user = User.objects.get_cached(obj.receiver_id)
        if not user:
            return ''
        if not user.nickname:
            return user.phone 
        return user.nickname
    
    def get_reply_by(self, obj):
        #return RegardSerializer(obj.reply_by).data
        return obj.reply_by.to_json() if obj.reply_by else {}
    
    
        
class PagingRegardSerializer(CustomPaginationSerializer):

    class Meta:
        object_serializer_class = RegardSerializer
        
        
class DateTipSerializer(BaseModelSerializer):
    
    spec_date = serializers.SerializerMethodField('str_spec_date_time')
    related_buddy = serializers.SerializerMethodField('get_related_buddy')
    
    class Meta:
        model = DateTip
        fields = ('id', 'creator', 'title', 'spec_date', 'notice_type', 'related_buddy', 'is_triggered', 'is_checked', 'desc')
        
        
    def str_spec_date_time(self, obj):
        return obj.spec_date.strftime('%Y-%m-%d %H:%M:%S')  
    
    def get_related_buddy(self, obj):
        return obj.related_buddy if obj.related_buddy else 0
        
    
    
class SystemMsgSerializer(BaseModelSerializer):
    
    receiver = serializers.SerializerMethodField('get_receiver')
    
    class Meta:
        model = SystemMsg
        fields = ('id', 'title', 'receiver', 'desc', 'type', 'is_checked', 'created_time')
        

    def get_receiver(self, obj):
        return obj.receiver_id if obj.receiver else 0    
                