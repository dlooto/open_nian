#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Jul 25, 2014, by Junn
#

from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$',                  views.RegardsAction.as_view(), ),
    url(r'^/send$',             views.RegardSendAction.as_view(), ),     #发问候 
    url(r'^/sent_list$',        views.RegardsSentList.as_view(), ),      #用户发送的问候列表
    url(r'^/received$',         views.RegardsReceivedList.as_view(), ),  #用户收到的问候列表
    
    url(r'^/(\d+)$',            views.RegardView.as_view(), ),
    url(r'^/(\d+)/check$',      views.RegardCheckView.as_view(), ),  #设置问候为已读
    
    url(r'^/tips$',             views.TipsAction.as_view(), ),
    url(r'^/tips/(\d+)$',       views.TipsAction.as_view(), ),
    url(r'^/tips/(\d+)/check$', views.check_datetip, ),
    url(r'^/sys_messages$',     views.get_system_messages, ),   #获取系统消息列表
    
    url(r'^/pull_messages$',    views.pull_new_messages, ),     #拉取新的问候消息
    
)