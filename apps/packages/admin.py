#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 28, 2014, by Junn
#

import settings

from django.contrib import admin
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template.response import TemplateResponse
from django.contrib import messages

from packages.models import AppPackage, DownloadLog
from django.core.exceptions import PermissionDenied
from utils import logs, eggs
import qiniu
import os

def disable_path(modeladmin, request, queryset):
    queryset.update(status='I')

def enable_path(modeladmin, request, queryset):
    queryset.update(status='A')

disable_path.short_description = u'禁用下载地址'
enable_path.short_description = u'启用下载地址'

access_key = settings.QINIU['access_key']
secret_key = settings.QINIU['secret_key']

bucket_name         = settings.QINIU['bucket_name']
android_url_prefix  = settings.QINIU['android_pkg_url']
ios_url_prefix      = settings.QINIU['ios_pkg_url']

class AppPackageAdmin(admin.ModelAdmin):
    list_display = ('id', 'os_type', 'pkgname', 'status', 'version', 'pkg_size', 
                    'change_log', 'desc', 'created_time' )
    list_filter = ('os_type', 'status',)
    exclude = ('status',)
    actions = [disable_path, enable_path]
    search_fields = ('version', 'pkg_num')
    change_list_template = 'admin/pkg_download_list.html'

    def get_urls(self):
        from django.conf.urls import patterns, url
        return patterns('',
            url(r'^upload/$', self.admin_site.admin_view(self.upload), name="packages_upload_pkg"),
        ) + super(AppPackageAdmin, self).get_urls()

    def save_model(self, request, obj, form, change):
        if not change: #新增数据
            AppPackage.objects.filter(os_type=obj.os_type, status='A').update(status='I')
        obj.save()

    def upload(self, request):
        if not self.has_change_permission(request):
            raise PermissionDenied
        if request.method == "POST":
            try:
                file = request.FILES.get('pkg_file')
                if not file:
                    self.message_user(request, message=u'请选择要上传的app包', level=messages.WARNING)
                    return upload_template_response(request, self)
                
                os_type = request.POST.get('os', 'A')
                version = file.name.split('-')[-1][0:-4]  #apk包或ipa包后续都为3个字符
                logs.info('===>file.size: %s' % file.size)
                
                is_test_pkg = settings.IS_TEST_PKG
                if not is_test_pkg:   # 上传包文件到正式包空间
                    if os_type == 'A':
                        file_url = '%s/%s' % (android_url_prefix, file.name)
                    else:
                        file_url = '%s/%s' % (ios_url_prefix, file.name)    
                    q = qiniu.Auth(access_key, secret_key)
                    token = q.upload_token(bucket_name, file_url)
                    
                    try:
                        #ret, info = qiniu.put_file(token, file_url, file.file.name, check_crc=True, mime_type="application/octet-stream", )  #this can't work when v1.8.0 uploaded, I don't know why
                        ret, info = qiniu.put_data(token, file_url, file.read(), check_crc=True, mime_type="application/octet-stream", )
                        assert info.status_code == 200
                    except Exception, e:
                        logs.err(__name__, eggs.lineno(), e)
                        self.message_user(request, message=u'%s' % e, level=messages.ERROR)
                        return upload_template_response(request, self)
                
                else:  #上传包文件到测试服务器
                    try:
                        with open('%s/%s' % (settings.TEST_PKG['upload_root'], file.name), 'wb+') as dest:
                            for chunk in file.chunks():
                                dest.write(chunk)
                            dest.close()    
                    except Exception, e:
                        logs.err(__name__, eggs.lineno(), e)       
                        dest.close()
                        self.message_user(request, message=u'测试包上传失败', level=messages.ERROR)
                        return upload_template_response(request, self)
                
                # create a new uploaded record
                #AppPackage.objects.filter(os_type=os_type, status='A').update(status='I')
                AppPackage.objects.create(
                    pkgname=file.name,
                    os_type=os_type,
                    version=version,
                    pkg_size=file.size,
                    change_log=request.POST.get('change_log', ''),
                    desc=request.POST.get('desc', ''),
                    is_test_pkg=is_test_pkg
                )
                
                self.message_user(request, message=u'上传成功', level=messages.SUCCESS)
                return HttpResponseRedirect(reverse('admin:packages_apppackage_changelist'))
            except Exception, e:
                logs.err(__name__, eggs.lineno(), e)
                self.message_user(request, message=u'上传失败, 请重新上传', level=messages.ERROR)
                
        return upload_template_response(request, self)
        
def upload_template_response(req, admin_model):
    return TemplateResponse(req, 'admin/upload_pkg.html',
                                context={'opts': admin_model.model._meta},
                                current_app=admin_model.admin_site.name)            


class DownloadLogAdmin(admin.ModelAdmin):
    list_display = ('os_type', 'pkgname', 'version','ip', 'agent', 'created_time')
    list_filter = ('os_type', 'version')
    search_fields = ('version', 'pkgname', 'pkg_num')


admin.site.register(AppPackage, AppPackageAdmin)
admin.site.register(DownloadLog, DownloadLogAdmin)