#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 28, 2014, by Junn
#

from core.models import BaseModel
from django.db import models

OS_CHOICES = (
    ('A', 'Android'),
    ('I', 'iOS'),
)

PACKAGE_STATUS = (
    ('A', u'可用'),
    ('I', u'禁用'),                      
)

VERSION_TYPE_CHOICES = (
    ('T', u'测试版本'),
    ('P', u'正式版本'), 
)

class AppPackage(BaseModel):
    '''app包模型'''
    os_type = models.CharField(u'平台类型', max_length=1, choices=OS_CHOICES, default='A')
    pkgname = models.CharField(u'app包名', max_length=50)
    version = models.CharField(u'app版本号', max_length=16)
    pkg_num = models.IntegerField(u'内部版本号', null=True, blank=True, default=1)
    status = models.CharField(u'状态', max_length=1, choices=PACKAGE_STATUS, default='A')
    
    pkg_size = models.IntegerField(u'app包大小') #包文件大小, 单位字节
    change_log = models.TextField(u'更新日志', max_length=800, default='')  #更新日志
    is_test_pkg = models.BooleanField(u'是否测试版本', default=True)      #
    desc = models.TextField(u'备注', max_length=100, default='')      #备注
    #version_type = models.CharField(u'版本类型', max_length=1, choices=VERSION_TYPE_CHOICES, default='T')  #版本类型, 

    def __unicode__(self):
        return '%s:%s, %s' % (self.os_type, self.pkgname, self.status)

    class Meta:
        verbose_name = u'app版本记录'
        verbose_name_plural = u'app版本记录'
        ordering = ('-version', '-pkg_num',)
        
    def is_new_version(self, version):
        '''比较版本号. 若当前obj版本号大于传入版本号, 返回True, 否则False.
        传入版本号形式如 2.0.1
        '''
        if self.version > version:
            return True
        if self.version < version:
            return False
    
class DownloadLog(BaseModel):
    '''app下载记录'''
    os_type = models.CharField(u'平台类型', max_length=1, choices=OS_CHOICES)
    ip = models.IPAddressField(u'下载IP', null=True, blank=True)
    agent = models.CharField(u'下载Agent', max_length=1024, null=True, blank=True)
    pkgname = models.CharField(u'app包名', max_length=50)
    version = models.CharField(u'程序版本号', max_length=16)
    pkg_num = models.IntegerField(u'内部版本号', default=0)

    def __unicode__(self):
        return '%s:%s' % (self.ip, self.os_type)

    class Meta:
        verbose_name = u'app下载日志'
        verbose_name_plural = u'app下载日志'    