#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 28, 2014, by Junn
#
import os
from django.http.response import HttpResponse, Http404, HttpResponseRedirect
from packages.models import AppPackage, DownloadLog
from core import codes
import settings
from core.decorators import login_required_mtd
from utils import logs, http
from rest_framework.decorators import api_view


@api_view(['GET'])
@login_required_mtd
def query_version(req):
    '''查询app包最新版本信息
    @param is_test_pkg: 若该参数值为'Y', 则表示查询测试安装包新版本, 否则查询正式包.
            服务端后期已调整为根据server本地的prod.py参数配置进行判断, 已去除该请求参数
    '''
    #return HttpResponseRedirect(settings.QUERY_VERSION_URL)
    
    os = req.REQUEST.get('os', 'A')
    #is_test_pkg = req.REQUEST.get('is_test_pkg', 'N')  #默认为正式版本
    
    if settings.IS_TEST_PKG:
        return get_test_pkg_version(os)
    
    version_short = ''     
    
    new = req.REQUEST.get('new', 'N')  #为了新旧app版本兼容, 添加该参数. 表示是否为新版本, 默认值N, 表示新版本
    if new == 'N':  # 若为旧版本
        send_url = '/api/v2/app/version/541a7131f880a6a814000087?token=xxxxxxxxx'
        try:
            response_str = http.send_request('fir.im', send_url, timeout=10)
            version_short = eval(response_str).get('versionShort')
        except Exception, e:
            logs.info(response_str)
            logs.error(u'旧版本请求versionShort异常: \n %s' % e)
            return http.failed('检查新版本异常')
          
    url = settings.QINIU['ios_pkg_url'] if os == 'I' else settings.QINIU['android_pkg_url']
    pkg = get_latest_pkg(os)        
    ctx = {
        'name':         pkg.pkgname,
        'version':      pkg.version,
        "versionShort": version_short,      #为与旧版本兼容保留该属性值, 从fir.im获取该值
        "short_no":     0,                  #内部版本号, 暂未使用(备用)  
        "pkg_size":     pkg.pkg_size,
        'changelog':    pkg.change_log,
        'update_url':   "%s/%s/%s" % (settings.PKG_DOWNLOAD_BASE_URL, url, pkg.pkgname),
    }
          
    return http.ok(ctx)

@api_view(['GET'])
def download_pkg(req):
    '''重定向到真实的app包下载地址, 进行下载
    当请求: http://dondon.im/download, 将进入该方法进行处理并重定向
    '''

    #nxx = req.REQUEST.get('nxx')
    #if not nxx == '5e4a0accc2bc9e0ef2c14e45ae15a767':
    #    return http.http404()
    
    os = req.GET.get('os', 'A')
    
    if os == 'A':
        url = settings.QINIU['android_pkg_url']
    else:
        url = settings.QINIU['ios_pkg_url']
            
    pkg = get_latest_pkg(os)
    log_download(req, pkg)
    
    try:
        return HttpResponseRedirect('%s/%s/%s' % (
            settings.PKG_DOWNLOAD_BASE_URL, url, pkg.pkgname))
    except Exception, e:
        logs.error('%s' % e)
        return HttpResponseRedirect(settings.ANDROID_DOWNLOAD_URL)
    
@api_view(['GET'])    
def download_test_pkg(req):    
    '''用于内部测试  ####################

    '''
    
    os_type = req.GET.get('os', 'A')
    pkg = get_latest_pkg(os_type, is_test_pkg=True)

    response = HttpResponse()
    response['X-Accel-Redirect'] = '%s/%s?pkgname=%s' % (
        settings.TEST_PKG['download_real_url'], pkg.pkgname, pkg.pkgname
    )
    return response    


def log_download(req, pkg):
    '''记录下载日志'''
    
    try:
        x_forwarded_for = req.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = req.META.get('REMOTE_ADDR')
        agent = req.META.get('HTTP_USER_AGENT')
        DownloadLog.objects.create(os_type=pkg.os_type, ip=ip, agent=agent, pkgname=pkg.pkgname, version=pkg.version)
    except Exception, e:
        logs.error('%s' % e)

def get_latest_pkg(os, is_test_pkg=False):
    '''获取最新的安装包'''
    
    if is_test_pkg:
        pkgs = AppPackage.objects.filter(os_type=os, status='A', is_test_pkg=True).order_by('-version')[:1]
        if not pkgs:
            raise Exception(u'还没有上传任何安装包')
        return pkgs[0]
    
    pkgs = AppPackage.objects.filter(os_type=os, status='A').order_by('-version')[:1]
    if not pkgs: 
        raise Exception(u'还没有上传任何安装包')
    return pkgs[0]

def get_test_pkg_version(os):
    pkg = get_latest_pkg(os, is_test_pkg=True)
    return http.ok({
        'name':         pkg.pkgname,
        'version':      pkg.version,
        "versionShort": 0,      
        "short_no":     0,                   
        "pkg_size":     pkg.pkg_size,
        'changelog':    pkg.change_log,
        'update_url':   settings.TEST_PKG['download_url'],  # 测试包下载需要一次重定向, 所以下载地址不需要给出包名 
    })

# def get_download_url1(req):    
#     '''重定向到真实的app包下载地址, 进行下载'''
#     os_type = req.GET.get('os', 'A')
#     app_pkg = AppPackage.objects.filter(os_type=os_type, status='A')[:1]
#     if not app_pkg:
#         raise Http404
#       
#     x_forwarded_for = req.META.get('HTTP_X_FORWARDED_FOR')
#     if x_forwarded_for:
#         ip = x_forwarded_for.split(',')[0]
#     else:
#         ip = req.META.get('REMOTE_ADDR')
#     agent = req.META.get('HTTP_USER_AGENT')
#     DownloadLog.objects.create(
#         os_type=os_type, ip=ip, agent=agent, version=app_pkg[0].version,
#         pkg_num=app_pkg[0].pkg_num
#     )
#       
#     response = HttpResponse()
#     response['X-Accel-Redirect'] = '%s%s?pkgname=%s' % (
#         settings.APP_DOWNLOAD_REAL_URL, app_pkg[0].pkgname, app_pkg[0].pkgname
#     )
#     return response

# @login_required_mtd
# def check_new_version(req):
#     '''
#     os为平台类型, os取值('A', 'O'), 'A'为Android, 'O'为iOS. 
#     传入版本号如: version='1.0.2_12'  
#     '''
#     os_type = req.REQUEST.get('os', 'A')
#     version = req.REQUEST.get('version', '')
#     if not os_type or not version:
#         return JResponse(codes.fmat('params_error', ': app操作系统类型或版本号为空'))
#     
#     pkg = AppPackage.objects.filter(os_type=os_type, status='A')[:1]
#     if not pkg:
#         return JResponse({'has_new': False})
#     
#     ctx = {}
#     if pkg[0].is_new_version(version):
#         ctx = {
#             'has_new': True,
#             'url': "%s%s" % (settings.APP_DOWNLOAD_URL, os_type),
#             'version':  pkg[0].version,
#             'pkg_num': pkg[0].pkg_num,
#         }
#     else:
#         ctx = {'has_new': False}
#         
#     ctx.update(codes.get('ok'))      
#     return JResponse(ctx)


