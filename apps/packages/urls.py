#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 28, 2014, by Junn
#

from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$',                          views.download_pkg, ),                
    url(r'^/query_version$',            views.query_version, ),  
    #url(r'^/check_new_version?$',      views.check_new_version, ),
    
    url(r'^/download_test$',            views.download_test_pkg, ),  #testing 
) 
