#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Sep 17, 2014, by Junn
#
from utils import logs

MSG_TYPE_CHOICES = (
    ('P', u'新动态提醒'),
    ('R', u'发送问候'),
    ('S', u'系统通知'),
    ('T', u'测试消息'),
)
    
# class Message(BaseModel):
#     msg_type = models.CharField(u'消息类型', max_length=1, choices=MSG_TYPE_CHOICES)
#     content = models.CharField(u'消息内容', max_length=1,)
#     send_to_list = models.CharField(u'接收者列表', max_length=1, default='', null=True, blank=True)  #默认发送给所有用户
#     desc = models.CharField(u'备注', max_length=100, default='', null=True, blank=True)

class PushMessage(object):
    '''推送消息体'''
    
    def __init__(self, _type, content, title='', extras={}):
        self.type = _type
        self.content = content
        self.title = title
        self.extras = extras
    

