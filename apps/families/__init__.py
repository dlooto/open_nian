#coding=utf-8
from utils import logs
 
try:
    from families.models import Family
    Family.objects.cache_all()
except ImportError, e:    
    print('ImportError: when cache_all families: \n %s' % e)
except Exception, e:
    print('Errors when cache_all families: \n %s' % e)   