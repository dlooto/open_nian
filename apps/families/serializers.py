#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 14, 2014, by Junn
#
from core.serializers import BaseModelSerializer, CustomPaginationSerializer
from families.models import Family
from rest_framework.fields import Field
from rest_framework import serializers
import settings

class FamilySerializer(BaseModelSerializer):
    
    class Meta:
        model = Family
        fields = ('id', 'name', 'creator', 'about', 'status')
        
class PagingFamilySerializer(CustomPaginationSerializer):   
    pass


class FamilyMemberSerializer(serializers.Serializer):  #记住: 使用Serializer很像Form. 
    '''此传入为FamilyMember类型对象, 所以继承Serializer, 而非ModelSerializer'''
    
    id = Field(source='id')
    phone = Field(source='get_user.phone')
    email = Field(source='get_user.email')
    nickname = Field(source='get_user.nickname')
    avatar = Field(source='get_user.avatar')
    birth = Field(source='get_user.birth')
    gender = Field(source='get_user.gender')
    city = Field(source='get_user.city')
    login_count = Field(source='get_user.login_count')
    info_completed = serializers.SerializerMethodField('is_info_completed') #资料是否完整
    
    family = serializers.SerializerMethodField('get_family_id')
    innername = Field(source='get_innername')
    family_perm = serializers.SerializerMethodField('get_family_perm')      #所有家庭权限, 目前仅是否管理员
    
    def get_family_id(self, obj):
        return obj.get_family.id if obj.get_family else 0   # 若无家庭则返回0
    
    def get_family_perm(self, obj):
        return 1 if obj.is_admin() else 0
    
    def is_info_completed(self, obj):
        return 0 if obj.get_user.pdu[2] == '0' else 1
    


