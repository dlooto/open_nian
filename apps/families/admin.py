#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 19, 2014, by Junn
#

from django.contrib import admin

from families.models import Family, FamilyShip
    
def mark_deleted(modeladmin, request, queryset):
    queryset.update(status='D')    

mark_deleted.short_description = u'设置为已删除'

class MemberInline(admin.TabularInline):
    model = FamilyShip
    extra = 1

class FamilyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'creator', 'created_time', 'status') # item list 
    list_filter = ('status', )
    search_fields = ('name', 'about', 'creator')
    fields = ('creator', 'name', 'about', ) #fields for creating new item 
    actions = [mark_deleted, ]
    inlines = (MemberInline, )
    ordering = ('id',) 


admin.site.register(Family, FamilyAdmin)