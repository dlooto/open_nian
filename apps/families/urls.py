#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 9, 2014, by Junn
#

from django.conf.urls import patterns, url

from families import views

urlpatterns = patterns(
    '',
    url(r'^$',      views.FamiliesAction.as_view(), ),
    url(r'^/(\d+)$',  views.FamilyView.as_view(), ),
    
    url(r'^/(\d+)/users$',       views.FamilyUsersAction.as_view(), ),
    url(r'^/(\d+)/users/(\d+)$',  views.FamilyUserView.as_view(), ),
    url(r'^/(\d+)/new_messages_count$',  views.pull_new_messages, ),
    
    #查询用户与家庭关系. 从手机通讯录直接选择联系人发送问候时, 会先调用该接口以确定问候接收用户与当前家庭关系
    url(r'^/(\d+)/get_membership$',   views.get_membership, ),  
)
