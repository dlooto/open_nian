#coding=utf8

"""
used for file process
"""

import os
import logs
import Image
from StringIO import StringIO
import settings
from utils import eggs


def is_file_exist(path):
    '''判断文件或目录是否存在'''
    if os.path.exists(path):
        return True
    return False


def remove(path, fileName=None):
    '''remove file from the filesystem'''
    if not fileName:
        fullpath = path
    else:
        fullpath = os.path.join(path, fileName)
        
    try:
        os.remove(fullpath)
        return True
    except OSError, e:
        logs.error("delete file %s error: %s" % (fullpath, e))
        return False


def save_audio(file, file_name):
    '''保存文件, 现仅用于保存语音文件
    @param file  传入的文件参数, request.FILES中获取的数据对象, file需要先经过rename处理, 以便获取到file.name
    @param file_name  保存的目标文件名
     '''
    
    if not file: return ''
    try:
        dest = open('%s/%s/%s' % (settings.MEDIA_ROOT, settings.AUD_DIR['original'], file_name), 'wb+')
        for chunk in file.chunks():
            dest.write(chunk)
        dest.close()
    except Exception, e:
        logs.error(e)        
        dest.close() 

    return file_name    