#coding=utf-8
#
# Copyright (C) 2014-2015  NianNian TECH Co., Ltd. All rights reserved.
# Created on Jan 8, 2015, by Junn
#
import datetime

##################################################
#  date and time processing utils
##################################################
def today():
    return datetime.date.today()


def yesterday():
    return today() + datetime.timedelta(days=-1)


def tomorrow():
    return today() + datetime.timedelta(days=1)


def weekday():
    '''获取今天星期几， 如，1对应星期一，4对应周期四，6对应周六等'''
    return datetime.date.isoweekday(datetime.date.today())


def monday():
    '''获取当前周星期一的日期'''
    d = today()
    delta = datetime.date.isoweekday(d) - 1
    return d - datetime.timedelta(days=delta)


def saturday():
    '''获取当前周周六的日期'''
    d = today()
    delta = 7 - datetime.date.isoweekday(d)
    return d + datetime.timedelta(days=delta - 1)


def sunday():
    '''获取当前周周日的日期'''
    d = today()
    delta = 7 - datetime.date.isoweekday(d)
    return d + datetime.timedelta(days=delta)


def afterDays(atime, interval):
    '''得到向后的某天时间'''
    return atime + datetime.timedelta(days=interval)


def beforeDays(atime, interval):
    '''得到向前的某天的时间, 如atime=now, interval=6, 则表示当前时间的前6天的那个时间'''
    return atime - datetime.timedelta(days=interval)