#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 19, 2014, by Junn
#

from django.contrib import admin

from feedbacks.models import Feedback
    
def mark_handled(modeladmin, request, queryset):
    queryset.update(status='D')    

mark_handled.short_description = u'设置为已处理'

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('id', 'content', 'created_time', 'ip', 'status') # item list 
    list_filter = ('status', )
    search_fields = ('content',)
    fields = ('contact', 'content', 'user', 'ip', 'status') #fields for creating new item 
    actions = [mark_handled, ]
    ordering = ('created_time',) 

admin.site.register(Feedback, FeedbackAdmin)