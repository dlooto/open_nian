#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 9, 2014, by Junn
#
from core.models import BaseModel
from django.db import models
from users.models import User
import settings

FEED_STATUS = (
    ('U', u'未处理'),
    ('D', u'已处理'),               
)

class Feedback(BaseModel):
    contact = models.CharField(u'联系方式', max_length=50, null=True, blank=True)
    content = models.CharField(u'反馈文字内容', max_length=512, null=True, blank=True)
    aud = models.FileField(verbose_name=u'反馈语音内容', null=True, blank=True, 
        upload_to=settings.AUD_FEEDBACK_DIR)
    
    user = models.ForeignKey(User, null=True, blank=True, verbose_name=u'用户')
    ip = models.IPAddressField(u'IP', null=True, blank=True)
    status = models.CharField(u'处理状态', max_length=1, choices=FEED_STATUS, default='U' )
    
    #reply = models.CharField(u'系统回复内容', max_length=512, null=True, blank=True)  #将系统回复的内容也记入该数据实体

    def __unicode__(self):
        return u'%s' % self.id

    class Meta:
        verbose_name = u'用户反馈'
        verbose_name_plural = u'用户反馈'