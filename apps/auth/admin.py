#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 19, 2014, by Junn
#

from django.contrib import admin
from auth.models import AuthedApp, CodeRecord, OpenAccount
from rest_framework.authtoken.models import Token

# from families.models import Family, FamilyShip
#     
# def mark_deleted(modeladmin, request, queryset):
#     queryset.update(status='D')    
# 
# mark_deleted.short_description = u'设置为已删除'
# 
# class MemberInline(admin.TabularInline):
#     model = FamilyShip
#     extra = 1
# 
# class FamilyAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name', 'creator', 'created_time', 'status') # item list 
#     list_filter = ('status', )
#     search_fields = ('name', 'about', 'creator')
#     fields = ('creator', 'name', 'about', ) #fields for creating new item 
#     actions = [mark_deleted, ]
#     inlines = (MemberInline, )
#     ordering = ('id',) 

class AuthedAppAdmin(admin.ModelAdmin):
    
    list_display = ('id', 'app_key', 'secret_key', 'created_time', 'is_valid') # item list 
    list_filter = ('is_valid', )
    search_fields = ('app_key',)
    fields = ('app_key', 'secret_key', 'is_valid', 'desc') #fields for creating new item 
    ordering = ('app_key',) 
        
class CodeRecordAdmin(admin.ModelAdmin):
    
    list_display = ('id', 'phone', 'code', 'action', 'is_valid', 'created_time') # item list 
    list_filter = ('is_valid', 'action')
    search_fields = ('phone', 'code', )
    fields = ('phone', 'code', 'action', 'is_valid', )
    ordering = ('created_time',) 
    
class OpenAccountAdmin(admin.ModelAdmin):
    
    list_display = ('user', 'source_site', 'openid', 'access_token', 'expires_in') # item list 
    list_filter = ('source_site', )
    search_fields = ('user__nickname', 'openid', )
    ordering = ('created_time',)     
    
class TokenAdmin(admin.ModelAdmin):
    
    list_display = ('key', 'user', 'created') # item list 
    #list_filter = ('', )
    search_fields = ('user__id', 'key', )  #仅search_fields可以使用xx__xx形式
    ordering = ('created',)     
    

admin.site.register(CodeRecord, CodeRecordAdmin)
admin.site.register(AuthedApp, AuthedAppAdmin)
admin.site.register(OpenAccount, OpenAccountAdmin)
admin.site.register(Token, TokenAdmin)