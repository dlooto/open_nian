#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Apr 21, 2014, by Junn
# 
#

from django.conf.urls import patterns, url

from auth import views

urlpatterns = patterns(
    '',
    url(r'^/get_token$', 'rest_framework.authtoken.views.obtain_auth_token'),   # get user token by username and password
    
    url(r'^/verify_phone$',  views.verify_phone,            name='auth_verify_phone'),
    url(r'^/check_code$',    views.check_sms_code,          name='auth_check_code'),
    url(r'^/bind_phone$',    views.bind_phone,              name='auth_bind_phone'), 
    
    url(r'^/signup$',        views.SignupAction.as_view(),  name='auth_signup'),
    url(r'^/signup1$',       views.signup1,                 name='auth_signup1'),
    url(r'^/login$',         views.LoginAction.as_view(),   name='auth_login'),
    url(r'^/open_login$',    views.OpenLogin.as_view(),     name='auth_open_login'),
    url(r'^/logout$',        views.logout,                  name='auth_logout'),
    
    url(r'^/change_password$', views.change_password,       name='auth_change_password'),
    url(r'^/reset_password$',  views.reset_password,        name='auth_reset_password'),
    
    #url(r'^/callback$',      views.OpenLoginView.as_view(),   name='auth_open_login'),
)
