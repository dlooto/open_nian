from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.conf.urls.static import static
import settings
from django.views.generic.base import TemplateView
# from filebrowser.sites import site

admin.autodiscover()

urlpatterns = patterns('',

    # Web home
    url(r"^$", TemplateView.as_view(template_name='index.html')),
    url(r"^n/about$", TemplateView.as_view(template_name='about.html')),
    url(r"^n/feedback", 'feedbacks.views.feedback'),
    
    
#     url(r"^grappelli/",     include("grappelli.urls")),   #this is needed for admin site
#     url(r"^admin/filebrowser/",   include(site.urls)),
    
    url(r'^admin/',   include(admin.site.urls)),
    url(r'^download',      include('packages.urls')),     # for app download
    

    url(r'^v1/auth',       include('auth.urls')),
    url(r'^v1/users',      include('users.urls')),
    url(r'^v1/families',   include('families.urls')),
    url(r'^v1/posts',      include('posts.urls')),
    url(r'^v1/regards',    include('regards.urls')),
    url(r'^v1/feedbacks',  include('feedbacks.urls')),
    url(r'^v1/packages',   include('packages.urls')),
    
)

if settings.DEBUG:
    # Used in debug mode for handling user-uploaded files
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += patterns("", url(r'^v1/tests', include('runtests.urls')))

