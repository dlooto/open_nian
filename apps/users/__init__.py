#coding=utf-8
from utils import logs, eggs

try:
    from users.models import User
    User.objects.cache_all()
except ImportError, e:    
    logs.err(__name__, eggs.lineno(), 'ImportError: when cache_all users \n %s' % e)
except Exception, e:
    logs.err(__name__, eggs.lineno(), 'Errors when cache_all users: \n %s' % e)


