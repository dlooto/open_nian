#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 19, 2014, by Junn
#

from django.contrib import admin
from posts.models import Post, Comment

class PostAdmin(admin.ModelAdmin):
    list_display = ('id','family', 'author', 'info', 'pic', 'aud', 'aud_len','created_time') 
    search_fields = ('family', 'author', 'info')
    fields = ('family', 'author', 'info', 'pic', 'aud', 'aud_len')   
    ordering = ('-created_time',)     
    
class CommentAdmin(admin.ModelAdmin):
    list_display = ('id','author', 'reply_to', 'post', 'info', 'aud','created_time')
    search_fields = ('info', 'author', 'post')
    fields = ('author', 'post', 'info', 'aud')   
    ordering = ('-created_time',)        

admin.site.register(Comment, CommentAdmin)
admin.site.register(Post, PostAdmin)