#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 9, 2014, by Junn
#

from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$',                      views.PostsAction.as_view(), ),
    #url(r'^/me$',                   views.PostsAction.as_view(), ),
    url(r'^/(\d+)$',                views.PostView.as_view(), ),
    
    url(r'^/(\d+)/comments$',       views.PostCommentsAction.as_view(), ),
    url(r'^/(\d+)/comments/(\d+)$', views.PostCommentView.as_view(), ),
    
    url(r'^/comments/rels$',        views.get_relcomment_msgs, ), #用户相关的评论或回复消息列表
    url(r'^/count_new_posts$',      views.start_count_posts_thread, ),
)