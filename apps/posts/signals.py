

from django.dispatch import Signal
 
# profileUpdated = Signal(providing_args=["request", "profile"])
# s_comment_success = Signal(providing_args=['comment'])
# s_care_success = Signal(providing_args=['care'])
# s_reply_success = Signal(providing_args=['reply'])
# s_clear_session = Signal(providing_args=['request', 'dataKey'])

s_post_added = Signal(providing_args=['user', 'family', 'post'])
# s_regard_push = Signal(providing_args=['regard'])
s_regard_waitlist_push = Signal(providing_args=['waitlist'])
s_comment_me = Signal(providing_args=['rel_to', 'comment'])
