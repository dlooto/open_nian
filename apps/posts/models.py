#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 9, 2014, by Junn
#

from core.models import BaseModel
from django.db import models
from users.models import User
from families.models import Family
from posts.managers import PostManager, CommentMsgManager
from utils import files, logs, eggs
from django.dispatch.dispatcher import receiver
from posts.signals import s_comment_me


class Post(BaseModel): #TODO: add status column for delete/normal
    '''家庭动态模型'''
    
    family = models.ForeignKey(Family,  verbose_name=u'所属家庭')
    author = models.ForeignKey('users.User',    verbose_name=u'发布者')
    #author_nickname = models.CharField(u'昵称', max_length=32, null=True, blank=True, default='')
    #author_avatar = models.CharField(u'头像', max_length=100, null=True, blank=True, default='')
    info = models.CharField(max_length=150, null=True, blank=True, verbose_name=u'文字', default='')
    
    # 当有多张图时, pic属性保存首张图片名, 其他图片默认以 xxx_1.jpg/xxx_2.jpg 命名
    pic = models.CharField(verbose_name=u'图片',max_length=80, null=True, blank=True, default='')
    pic_num = models.IntegerField(u'图片张数', null=True, blank=True, default=0)   #上传多张图时需要该属性, 最多9张图
    
    aud = models.CharField(verbose_name=u'语音', max_length=80, null=True, blank=True, default='')
    aud_len = models.IntegerField(u'语音长度', null=True, blank=True, default=0) #单位秒
    
    objects = PostManager()
    
    comments = []
    
    class Meta:
        verbose_name = u'家庭动态'
        verbose_name_plural = u'家庭动态'
        
    def __unicode__(self):
        return u'%s' % self.id
    
    def get_all_comments(self):
        return self.comment_set.all()
    
    def add_comment(self, data):
        comment = Comment(**data)
        comment.post = self
        comment.aud = files.save_audio(data['aud'], eggs.gen_uuid1())
        comment.save()
        return comment
    
    
class Comment(BaseModel):
    '''评论/评论回复模型. 目前仅允许单独的文字或语音评论'''
    
    author = models.ForeignKey('users.User', related_name='cauthor', verbose_name=u'评论人')  
    
    #为空时表示实体对象为评论
    reply_to = models.ForeignKey('users.User', related_name='reply_to', null=True, blank=True, verbose_name=u'被回复对象')  
    post = models.ForeignKey(Post, verbose_name=u'所评论的动态')
    info = models.CharField(max_length=150, null=True, verbose_name=u'文字评论', default='')
    aud = models.CharField(verbose_name=u'语音评论', max_length=80, null=True, blank='', default='')
    aud_len = models.IntegerField(u'语音长度', null=True, blank=True, default=0) #单位秒
    
    class Meta:
        verbose_name = u'评论'
        verbose_name_plural = u'评论'
        
    def __unicode__(self):
        return u'%s' % self.id
    
    
class CommentMsg(BaseModel):
    '''与某用户相关的评论或回复消息: 表示评论某用户的动态被评论或评论被人回复 '''
    
    rel_to = models.ForeignKey('users.User', verbose_name=u'与谁有关') #被评论动态的发布者, 或者评论被回复者
    comment = models.ForeignKey(Comment, verbose_name=u'关联的评论或回复')
    is_checked = models.BooleanField(default=False, verbose_name=u'是否已读')  
    
    objects = CommentMsgManager()
    
    class Meta:
        verbose_name = u'评论消息'
        verbose_name_plural = u'评论消息'
        ordering = ['-created_time']
        
    def __unicode__(self):
        return u'%s' % self.id    
    

@receiver(s_comment_me)
def create_commentmsg(sender, **kwargs):
    logs.info(kwargs)
    rel_to = kwargs.pop('rel_to', None)
    comment = kwargs.pop('comment', None)
    if not rel_to or not comment:
        return
    
    cmsg = CommentMsg(rel_to=rel_to, comment=comment)
    cmsg.save()
        
    
    
    
