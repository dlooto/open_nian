#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 14, 2014, by Junn
#
from core.serializers import BaseModelSerializer, CustomPaginationSerializer
from rest_framework.fields import Field
from posts.models import Post, Comment, CommentMsg
from rest_framework import serializers
import settings
from utils import logs
from users.models import User


class PostSerializer(BaseModelSerializer):
    #pic = serializers.SerializerMethodField('get_pic')
    author_nickname = serializers.SerializerMethodField('get_author_nickname')
    author_avatar = serializers.SerializerMethodField('get_author_avatar')
    comments = serializers.SerializerMethodField('get_comments')
    
    class Meta:
        model = Post
        fields = (
            'id', 'family', 'author', 'author_nickname', 'author_avatar', 
            'info', 'pic', 'pic_num', 'aud', 'aud_len', 'created_time', 'comments'
        )
#     ordering = ('created_time', )
        
    def get_author_nickname(self, obj):
        user = User.objects.get_cached(obj.author_id)
        return user.nickname if user else ''
    
    def get_author_avatar(self, obj):
        user = User.objects.get_cached(obj.author_id)
        return user.avatar if user else ''
    
    def get_comments(self, obj):
        return CommentSerializer(obj.comments).data
        
        
class PagingPostSerializer(CustomPaginationSerializer):

    class Meta:
        object_serializer_class = PostSerializer


class CommentSerializer(BaseModelSerializer):
    author_nickname = serializers.SerializerMethodField('get_author_nickname')
    author_avatar = serializers.SerializerMethodField('get_author_avatar')
    reply_to_nickname = serializers.SerializerMethodField('get_reply_to_nickname')
    reply_to = serializers.SerializerMethodField('get_reply_to')
    
    class Meta:
        model = Comment
        fields = ('id', 'post', 'author', 'author_nickname', 'author_avatar', 'reply_to', 'reply_to_nickname', 'info', 'aud', 'aud_len', 'created_time')
        
    def get_aud(self, obj):
        return '%s%s' % (settings.MEDIA_URL, obj.aud) if obj.aud else ''   
    
    def get_author_avatar(self, obj):
        user = User.objects.get_cached(obj.author_id)
        return user.avatar if user else ''
    
    def get_author_nickname(self, obj):
        user = User.objects.get_cached(obj.author_id)
        return user.nickname if user else ''  
    
    def get_reply_to(self, obj):
        return 0 if not hasattr(obj, 'reply_to') or not obj.reply_to else obj.reply_to_id  #here, we can't use <if obj.reply_to>, or self.field.rel.to.DoesNotExist would be raised
    
    def get_reply_to_nickname(self, obj):
        if not hasattr(obj, 'reply_to') or not obj.reply_to:
            return ''
        user = User.objects.get_cached(obj.reply_to_id)
        return user.nickname if user else ''
   
class CommentMsgSerializer(BaseModelSerializer):
    author_nickname = serializers.SerializerMethodField('get_author_nickname')
    author_avatar = serializers.SerializerMethodField('get_author_avatar')

    cmt_info = serializers.SerializerMethodField('get_cmt_info')        #评论文字内容
    cmt_aud = serializers.SerializerMethodField('get_cmt_aud')          #评论语音内容
    cmt_aud_len = serializers.SerializerMethodField('get_cmt_aud_len')  #评论语音长度

    post = serializers.SerializerMethodField('get_post')    
    post_info = serializers.SerializerMethodField('get_post_info')
    post_pic = serializers.SerializerMethodField('get_post_pic')
    
    class Meta:
        model = CommentMsg
        fields = ('id', 'author_avatar', 'author_nickname', 'cmt_info', 'cmt_aud', 'cmt_aud_len', 
                  'post', 'post_pic', 'post_info', 'created_time')
        
    def get_author_avatar(self, obj):
        user = User.objects.get_cached(obj.comment.author_id)
        return user.avatar if user else ''
    
    def get_author_nickname(self, obj):
        user = User.objects.get_cached(obj.comment.author_id)
        return user.nickname if user else ''  
    
    def get_cmt_info(self, obj):
        return obj.comment.info if obj.comment else ''    
    
    def get_cmt_aud(self, obj):
        return obj.comment.aud if obj.comment and obj.comment.aud else ''
        
    def get_cmt_aud_len(self, obj):
        return obj.comment.aud_len if obj.comment and obj.comment.aud else 0        
    
    def get_post(self, obj):
        return obj.comment.post if obj.comment.post else ''
        
    def get_post_info(self, obj):
        return obj.comment.post.info if obj.comment.post else ''
    
    def get_post_pic(self, obj):  
        return obj.comment.post.pic if obj.comment.post else ''        
        
        
class PagingCommentSerializer(CustomPaginationSerializer):
    class Meta:
        object_serializer_class = CommentSerializer        
        
        