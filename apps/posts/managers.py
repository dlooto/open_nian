#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on May 9, 2014, by Junn
#
from core.managers import BaseManager
from utils import logs

class PostManager(BaseManager):
    def get_user_published(self, user, item_time=None, limit=10):
        qs = self.filter(author=user)
        if item_time:
            return qs.filter(created_time__lt=item_time).order_by('-created_time')[:limit]
        return qs.order_by('-created_time')[:limit]

class CommentMsgManager(BaseManager):
    
    def get_relcomment_msgs(self, user, last_time=None, limit_count=10):
        '''返回某用户未读的相关评论/回复消息列表
        
        @param user: 指定用户对象
        '''
        msgs = self.filter(rel_to=user, is_checked=False)
        if last_time:
            return msgs.filter(created_time__lt=last_time)[:limit_count]
        return msgs[:limit_count]
    
    def check_msgs(self, user):
        '''将某用户未读的相关评论/回复消息置为已读
        
        @param user: 指定用户对象
        '''
        self.filter(rel_to=user, is_checked=False).update(is_checked=True)
    
    def get_checked_relcomment_msgs(self, user, last_time=None, limit_count=10):
        '''返回某用户已读的相关评论/回复消息列表
        @param user: 指定用户对象
        '''
        msgs = self.filter(rel_to=user, is_checked=True)
        if last_time:
            return msgs.filter(created_time__lt=last_time)[:limit_count]
        return msgs[:limit_count]            
    
    def get_all_relcomment_msgs(self, user, last_time=None, limit_count=10):
        '''返回某用户相关的所有评论/回复消息, 包括已读和未读'''
        msgs = self.filter(rel_to=user)
        if last_time:
            return msgs.filter(created_time__lt=last_time)[:limit_count]
        return msgs[:limit_count]
        
    def count_relcomment_msgs(self, user):
        return self.get_relcomment_msgs(user).count()

        
        