#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Mar 23, 2014, by Junn
# 
#
from django.db import models

class City(models.Model):
    name = models.CharField(u'城市名称', max_length=16)
    
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'城市'
        verbose_name_plural = u"城市"
        
class County(models.Model):
    name = models.CharField(u"区域名称", max_length=16)
    city = models.ForeignKey(City, verbose_name=u"城市")

    def __unicode__(self):
        return '%s,%s' % (self.city.name, self.name)

    class Meta:
        verbose_name = u'行政区'
        verbose_name_plural = u"行政区"        