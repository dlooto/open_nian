#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Sep 24, 2014, by Junn
#

'''A sample for thread pool

'''

import Queue
import threading
import time
from utils import logs

class WorkManager(object):
    def __init__(self, job_num=1000, thread_num=2):
        self.job_queue = Queue.Queue()
        self.threads = []
        #self.__init_job_queue(job_num)
        self.__init_thread_pool(thread_num)

    """初始化线程"""
    def __init_thread_pool(self,thread_num):
        for i in range(thread_num):
            self.threads.append(JobThread(self.job_queue))

    """初始化工作队列"""
#     def __init_job_queue(self, jobs_num):
#         for i in range(jobs_num):
#             self.add_job(self.job_name, i)

    """添加一项工作入队"""
    def add_job(self, func, *args):
        self.job_queue.put((func, list(args))) #任务入队，Queue内部实现了同步机制
    
    """检查剩余队列任务"""
    def check_queue(self):
        return self.job_queue.qsize()

    """等待所有线程运行完毕"""   
    def wait_allcomplete(self):
        for item in self.threads:
            if item.isAlive(): item.join()

class JobThread(threading.Thread):
    def __init__(self, job_queue):
        threading.Thread.__init__(self)
        self.job_queue = job_queue
        self.start()

    def run(self):
        while True:
            try:
                func, args = self.job_queue.get(block=False) #任务异步出队，Queue内部实现了同步机制
                func(args)
                self.job_queue.task_done() #通知系统任务完成
                print 'task done, current_thread: %s' % threading.current_thread()
            except Exception, e:
                print 'do_job error : \n %s' % e


def do_job(args):
    '''示例: 具体要做的任务'''
    print args
    time.sleep(0.1) #模拟处理时间
    print threading.current_thread(), list(args)

''' 线程池示例运行 '''
if __name__ == '__main__':
    start = time.time()
    work_manager =  WorkManager(10, 2)#或者work_manager =  WorkManager(10000, 20)
    work_manager.wait_allcomplete()
    end = time.time()
    print "cost all time: %s" % (end-start)