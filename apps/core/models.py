#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Mar 21, 2014, by Junn
# 
#

from django.db import models

from managers import BaseManager


class BaseModel(models.Model):
    created_time = models.DateTimeField(u'创建时间', auto_now_add=True)

    objects = BaseManager()

    class Meta:
        abstract = True
        ordering = ['-created_time']
