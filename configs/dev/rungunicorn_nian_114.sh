#!/bin/bash
set -e

cd /home/ada/prod/nian/apps

# activate the virtualenv
source /opt/envs/nian/bin/activate

# start gunicorn with all options earlier declared in fabfile.py

exec python manage.py run_gunicorn -w 4 \
    --user=ada --group=ada \
    --settings=settings.prod114 \
    --bind=0.0.0.0:8000 \
    --log-level=info \
    --log-file=/home/ada/prod/nian/logs/nian_gunicorn.log 2>>/home/ada/prod/nian/logs/nian_gunicorn.log

    
