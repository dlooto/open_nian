#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on 2014-5-19, by Junn
#

import os
from fabric.api import env, task, cd, puts, abort, lcd, hide
from fabric.operations import sudo, run, local, open_shell, settings, put
from fabric.contrib.console import confirm
from fabric.contrib import console
from fabric.contrib.files import exists
from fabric.colors import _wrap_with, green

green_bg = _wrap_with('42')
red_bg = _wrap_with('41')


@task
def nian():
    #env.local_repos_root = '/Users/junn/work/workspace/'           # On Mac
    
    env.data_root = '/home/ada/data/nian'
    
    env.prod_root = '/home/ada/prod'
    
    env.nian_home = '%s/%s' % (env.prod_root, 'nian')
    env.src_root = '%s/%s' % (env.nian_home, 'apps')  #product code data_root
    env.scripts_dir = '%s/%s' % (env.nian_home, 'scripts')
    
    env.python_path = '/opt/envs/nian/bin'
    
    env.backup_dir = '%s/%s' % (env.data_root,   'backup/projects')
    env.media_root = '%s/%s' % (env.data_root,   'media')
    env.logs_root = '%s/%s' % (env.data_root,    'logs')
    
    env.remote_repos = 'git@git.oschina.net:xjbean/nian.git'
    
    

    #env.db_schema = "nian"
    #env.db_user = "root"
    #env.db_password = "Nian2014@n"
    #env.scripts_name = ''

    env.ask_confirmation = True
    
@task
def nian110():
    #env.local_repos_root = '/Users/junn/work/workspace/'           # On Mac
    
    env.data_root = '/home/ada/data/nian'
    
    env.hosts = ['nian@192.168.0.110']
    env.prod_root = '/home/ada/prod'
    
    env.nian_home = '%s/%s' % (env.prod_root, 'nian')
    env.src_root = '%s/%s' % (env.nian_home, 'apps')  #product code data_root
    env.scripts_dir = '%s/%s' % (env.nian_home, 'scripts')
    
    env.python_path = '/opt/envs/nian/bin'
    
    env.backup_dir = '%s/%s' % (env.data_root,   'backup/projects')
    env.media_root = '%s/%s' % (env.data_root,   'media')
    env.logs_root = '%s/%s' % (env.data_root,    'logs')
    
    env.remote_repos = 'ssh://git@192.168.0.110:7999/nn/nian.git' 
    
    

    #env.db_schema = "nian"
    #env.db_user = "root"
    #env.db_password = "Nian2014@n"
    #env.scripts_name = ''

    env.ask_confirmation = True    


#################################################################################################
@task
def upgrade():
    '''备份, 更新源代码, DB同步, 执行sql脚本, 添加新库/包'''
    test_configuration()

    #stop_site()             #停止站点
    #backup_code()           #备份工程代码
    update_code()           #更新工程
    prepare_settings()
    create_dir_links()
    #collect_static()
    update_db_schema()      #执行syncdb及sql脚本更新
    

@task
def quick():
    '''仅更新源代码'''
    test_configuration()
    
    backup_code()
    update_code()
    prepare_settings()
    collect_static()
    create_dir_links()


@task
def backup_code():
    with cd(env.backup_dir):
        with settings(hide('warnings','stderr','stdout','running'), warn_only=True):
            sudo('mv nian nian-`%Y%m%d`')
    with cd(env.prod_root):    
        sudo('cp -r nian %s' % env.backup_dir)

@task
def update_code():
    if not exists(env.nian_home):
        with cd(env.prod_root):
            run('git clone %s' % env.remote_repos)
    else:
        with cd(env.nian_home):
            run('git pull')

@task 
def collect_static():
    with cd(env.src_root):
        run('%s/python manage.py collectstatic' % env.python_path)

@task
def create_dir_links():
    '''make media link'''
    with cd(env.data_root):
        media_link = '%s/media' % env.nian_home
        logs_link = '%s/logs' % env.nian_home
        if exists(media_link):                #this exists function is not available
            sudo('rm -rf %s' % media_link)
        if exists(logs_link):
            sudo('rm -rf %s' % logs_link)
    with cd(env.nian_home):        
        run('ln -s %s %s' % (env.media_root, media_link))
        run('ln -s %s %s' % (env.logs_root, logs_link))

@task
def prepare_settings():
    '''准备server运行配置'''
    with cd(env.src_root):
        if not exists('./settings/__init__.py'):
            run('touch ./settings/__init__.py')
        run('''echo 'from prod import *' > ./settings/__init__.py ''')

@task
def update_db_schema():       
    with cd(env.src_root):
        run('%s/python manage.py syncdb --settings=settings.prod' % env.python_path)
        
        #cmd = 'SOURCE scripts/%s' % env.scripts_name 
        #run('''mysql -u %s -p%s %s -e "%s"''' % (env.db_user, env.db_password, env.db_schema, cmd))

@task 
def backup_db(): 
    #提前单独做这一步,不放在upgrade中进行    
    pass

def is_link_exists(source):
    """ Determine if a file is a symlink """
    with settings(hide('warnings','stderr','stdout','running'), warn_only=True):
        return run("test -L '%s' && echo OK ; true" % source) == "OK"


@task
def test_configuration():
    params = []
    
    params.append(('Remote Data root',      env.data_root))
    params.append(('Remote Hosts',          env.hosts))
    params.append(('Remote media root',     env.media_root))
    params.append(('Remote logs root',      env.logs_root))
    params.append(('Remote Backup dir',     env.backup_dir))
    params.append(('Remote Python PATH',    env.python_path))
    params.append(('Remote Project Home',   env.nian_home))
    params.append(('Remote Source root',    env.src_root))
    params.append(('', '----'))
    #params.append(('DB schema', env.db_schema))
    #params.append(('DB user', env.db_user))
    #params.append(('DB password', env.db_password))
    
    for param in params:
        param_format = "'%s'" if isinstance(param[1], str) else "%s"
        param_value = param_format % param[1]
        puts('%s %s' % (param[0].ljust(27), green(param_value)))

    if env.ask_confirmation:
        if not console.confirm("Are you sure you want to upgrade %s?" % red_bg('dondon.im'), default=False):
            abort("Aborting at user request.")
    puts(green_bg('Start upgrade dondon.im ...'))


