#!/bin/bash

## use this script to run or stop online site

COMMAND=$1

PROD_ROOT='/home/ada/prod/nian'

NGX_CONF_ADMIN='nian-admin.conf'
NGX_CONF_APP='nian-app.conf'
NGX_CONF_WEB='nian-web.conf'
NGX_CONF_DOWNLOAD='nian-downloads.conf'
NGX_CONF_MAINTAIN='nian-maintain.conf'


if [ "$COMMAND" = "stop" ] ; then
    sudo rm /etc/nginx/sites-enabled/nian-*.conf
    sudo ln -s $PROD_ROOT/configs/prod/$NGX_CONF_MAINTAIN  	/etc/nginx/sites-enabled/$NGX_CONF_MAINTAIN
elif [ "$COMMAND" = "run" ] ; then
    sudo rm /etc/nginx/sites-enabled/$NGX_CONF_MAINTAIN
    sudo ln -s $PROD_ROOT/configs/prod/$NGX_CONF_ADMIN 		/etc/nginx/sites-enabled/$NGX_CONF_ADMIN
    sudo ln -s $PROD_ROOT/configs/prod/$NGX_CONF_APP 		/etc/nginx/sites-enabled/$NGX_CONF_APP
else
    echo "$COMMAND : comman not found"
    exit 1
fi

sudo service nginx restart
