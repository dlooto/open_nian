#!/bin/bash
set -e

cd /home/ada/prod/nian/apps
source /opt/envs/nian/bin/activate
exec python manage.py crontab add